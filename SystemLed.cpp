/**
 * Represents a LED to display simple information.
 * Modes are:
 * 1) on, off
 * 2) continued blinking with variable frequency
 * 3) one time blink code.
 *
 * Tick time should be 100ms
 *
 * @todo should be extended by a class that knows the states of esknodo and how to display them
 */
#include <Arduino.h>
#include "SystemLed.h"


/**
 * Set port to output, init state of port, set logic inversion state.
 */
SystemLed::SystemLed(int port, bool inverted)
{
  this->port = port;
  this->logicLevelInverted = inverted;
  // LED setup
  pinMode(port, OUTPUT);
  digitalWrite(port, this->logicLevelInverted);
}

/**
 * Applies logic level inversion
 */
bool SystemLed::logicLevel(bool level)
{
    return level ^ this->logicLevelInverted;
}

/**
 * Turns the Led on and all internal counters off.
 */
bool SystemLed::on()
{
  this->ledOnTime = __INT_MAX__;
  this->ledOffTime = 0;
  this->blinkMode = 0;
  this->blinkCodeCount = 0;
  this->waitAfterBlinkTime = 0;
  return true;
}

/**
 * Turns the Led and all internal counters off.
 */
bool SystemLed::off()
{
  this->ledOnTime = 0;
  this->ledOffTime = 0;
  this->blinkMode = 0;
  this->blinkCodeCount = 0;
  this->waitAfterBlinkTime = 0;
  return true;
}

/**
 * Sets the Led to one single shine up.
 */
bool SystemLed::setSingleOn(int onTime)
{
    return this->setSingleOn(onTime, 10);
}

/**
 * Sets the Led to one single shine up.
 */
bool SystemLed::setSingleOn(int onTime, int offTime)
{
    this->ledOnTime = onTime;
    this->ledOffTime = offTime;
    this->blinkMode = 0;
    this->blinkCodeCount = 0;
    this->waitAfterBlinkTime = 0;
    return true;
}

/**
 * Sets the Led to constant, endless blinking.
 * Blink code cannot be used then.
 * May be reset by writing setBlinking(0)
 * The parameter is the half cycle length of blinking (unit of 100ms, e.g. a value of 4 gives T=800ms -> 1.25Hz).
 */
bool SystemLed::setBlinking(int blinkMode)
{
  this->blinkMode = blinkMode;
  this->blinkCodeCount = 0;
  this->waitAfterBlinkTime = 0;
  return false;
}

/**
 * Sets a number of countable Led flashes.
 * Returns false if Led is not ready.
 * Every blink mode can be set at any time, except a blink code.
 * If a blink code display is in progress, the new blink code is
 * memorized. By that, it is assured that the last given blink
 * code is always displayed.
 * Additionaly, if setting a blink code continously, the display
 * of blink code is not disturbed.
 */
bool SystemLed::setBlinkCode(int blinkCode)
{
    this->blinkMode = 0;
    if (this->isReady()) {
        this->pauseBefore = 12;
        this->blinkCodeCount = blinkCode;
        this->waitAfterBlinkTime = 12;
        return true;
    }
    else {
        this->queuedBlinkCode = blinkCode;
        return false;
    }
}

/**
 * Sets an error code.
 * An error code runs continous.
 * Each sequence of an error code is preceeded by a flicker.
 */
bool SystemLed::setErrorCode(int errorCode)
{
    this->blinkMode = 0;
    this->pauseBefore = 12;
    this->flashBeforeBlink = 10;
    this->blinkCodeCount = errorCode;
    this->waitAfterBlinkTime = 12;
    return true;
}

/**
 * Tells if a blink code is running.
 * A blink code must have been finished before setting a new one.
 */
bool SystemLed::isReady()
{
  if (this->waitAfterBlinkTime || this->blinkCodeCount) {
    return false;
  } else {
    return true;
  }
}

/**
 * Counters for Led on and off time and a wait time after blinking
 * are decrementd until zero. Then the Led is ready for
 * the next flash.
 */
void SystemLed::routine100ms()
{
  bool ledState = false;

  if (this->ledOnTime) {
     this->ledOnTime --;
     ledState = true;
  }
  else if (this->ledOffTime) {
     this->ledOffTime --;
     ledState = false;
  }
  else if (this->blinkMode) {
     this->ledOnTime = this->blinkMode;
     this->ledOffTime = this->blinkMode;
  }
  else if (this->blinkCodeCount) {
    // process next state
    if (this->flashBeforeBlink) {
      if (this->flashBeforeBlink & 1) {
        ledState = true;
      }
      else {
        ledState = false;
      }
      this->flashBeforeBlink --;
    }
    else if (this->pauseBefore) {
        ledState = false;
        this->pauseBefore --;
    }
    else {
      this->ledOnTime = 4;
      this->ledOffTime = 6;
      this->blinkCodeCount --;
    }
  }
  else if (this->waitAfterBlinkTime) {
    this->waitAfterBlinkTime --;
    ledState = false;
  }
  else if (this->queuedBlinkCode) {
    this->blinkCodeCount = this->queuedBlinkCode;
    this->queuedBlinkCode = 0;
  }
  else {
    ledState = false;
  }

  if (ledState) {
     digitalWrite(this->port, this->logicLevel(HIGH));
  }
  else {
     digitalWrite(this->port, this->logicLevel(LOW));
  }
}

/**
 * An alternative to routine1ms()
 */
void SystemLed::loop()
{
  static unsigned long lastMillis;
  if (millis() % 64 == 0) {
    this->routine100ms();
  }
  lastMillis = millis();
}
