/**
 * Example of hardware- and project specific settings.
 */

#define PROJECT_NAME "Esknodo"


/**
 * If true, the wifi portal is activated after x seconds of
 * not beeing connected to wifi.
 * If no wifi was connected ever, portal mode opens in every case.
 * Recommended: false
 */
#define WIFI_PORTAL_AUTOMATIC             false
#define WIFI_PORTAL_WAIT_FOR_WIFI_SECONDS 15
#define WIFI_PORTAL_ACTIVE_SECONDS        120

#define WIFI_UNCONNECTED_TIMEOUT   15
#define MQTT_UNCONNECTED_TIMEOUT   15

// Esknodo1 @ ESP12E+F  pinout
// Busses
#define PORTPIN_I2C_SCL 5
#define PORTPIN_I2C_SDA 4

// LEDs / Keys
#define PORTPIN_FLASH 0

// Moisture Sensor
#define PORTPIN_SENSOR_1       12
#define PORTPIN_SENSOR_2       13
#define PORTPIN_SENSOR_3       14
#define PORTPIN_SENSOR_SUPPLY  15

// Battery voltage
#define PORTPIN_ADC0           17

