/**
 * A battery powered plant sensor to monitor ambient parameters, especially soil moisture.
 * Sensor data is passed via mqtt over wifi.
 *
 * Based on various code examples
 *
 * ArduinoJson
 * @see https://arduinojson.org/v6/api/jsondocument/
 * @see https://arduinojson.org/v6/assistant/
 *
 * For Eeprom lib, @see https://github.com/jwrw/ESP_EEPROM
 *
 * For WiFi-Settings, @see https://github.com/Juerd/ESP-WiFiSettings
 *
 * For Button lib, @see https://github.com/mathertel/OneButton
 *
 * For Battery lib, @see https://github.com/LennartHennigs/ESPBattery
 *
 * For (currently unused) OneWire lib, @see https://gitea.youb.fr/youen/OneWireArduinoSlave
 *
 * Android app: @see https://www.snrlab.in/iot/iot-mqtt-panel-user-guide/
 *
 */
#include <inttypes.h>
#include <Arduino.h>

#include "esknodo.h"
#include "install/hardware.h"
#include "MoistureSensor1.h"
#include "MoistureSensor2.h"
#include "SystemLed.h"
#include "Mqtt.h"
#include "SleepManager.h"
#include "Store.h"

#include <Wire.h>
#include <Ticker.h>
#include <LittleFS.h>
#include "ESP8266WiFi.h"
#include <WiFiSettings.h>
#include <ESPBattery.h>
#include <OneButton.h>
#include <I2CSoilMoistureSensor.h>

// AM2320 code
#include "Adafruit_Sensor.h"
#include "Adafruit_AM2320.h"

#ifndef LED_BUILTIN
#error The level of led on and off must be defined
#endif

#ifndef LED_BUILTIN_INVERTED
#error The polarity of level logic of led must be defined
#endif

#ifndef PORTPIN_SENSOR_SUPPLY
#error The pin number of the bus power supply switch must be defined
#endif


/**
 * Developer options are use for development and debugging.
 * Usage: e.g.: developOptions = DevShortSleep | DevMoreVerbose | DevSensorVerbose;
 * Developer options can be overridden by develop options from EEStore (EEStore.data.developOptions).
 * If options are set in EEStore, the value from EEStore is copied to this variable
 */
int developOptions = DevNoDevelopmentOptions | DevSensorVerbose;


//Wire.begin(SDA_PIN, SCL_PIN, I2C_MASTER);


Ticker systemTicker1;
Ticker systemTicker100;

/**
 * Battery
 * Startup configuration
 */
ESPBattery Battery(
  PORTPIN_ADC0,
  100,
  ESPBATTERY_CRITICAL,
  ESPBATTERY_FULL_HIGH
);

/**
 * Permanent EEPROM store
 * Usage: Store.data.VARNAME
 */
Store EEStore;

// Esknodo2 has 2 sensors attached
MoistureSensor1 Sensor0(PORTPIN_SENSOR_1, 0);
MoistureSensor1 Sensor1(PORTPIN_SENSOR_2, 1);
MoistureSensor2 Sensor2(PORTPIN_SENSOR_3, 2);

// I2C Soil Moisture Sensor
I2CSoilMoistureSensor CatnipSensor;

// Flash Button
OneButton FirstButton = OneButton(
  PORTPIN_FLASH,  // Input pin for the button
  true,    // true = Button is active LOW
  true     // true = activate internal pull-up resistor
);
// System LED displays basic states of Esknodo
SystemLed LedSystem(LED_BUILTIN, LED_BUILTIN_INVERTED);

// Mqtt
Mqtt MqttConnection;
String mqttServer = "";
uint16_t mqttPort = 0;
String mqttClientID = "";
String mqttUsername = "";
String mqttPassword = "";
/** This is the individual topic prefix of an esknodo instance (preloaded with an example) */
String mqttTopic = String(MQTT_TOPIC_DEFAULT);
// mqttQOS resides in Mqtt.h

// Manages wakeup cycles and sleep duration
SleepManager SleepMgr;

// AM2320 temperature and air moisture sensor
Adafruit_AM2320 am2320 = Adafruit_AM2320();

bool wifiReady = false;
bool mqttReady = false;
#define ESTATE_STARTED 1
#define ESTATE_SERIAL1 2
#define ESTATE_BATTERY 3
#define ESTATE_SERIAL2 4
#define ESTATE_DEV_OPTIONS 5
#define ESTATE_TICKERS 6
#define ESTATE_BUTTONS 7
#define ESTATE_SLEEP_CYCLES 8
#define ESTATE_SENSORS 9
#define ESTATE_I2C 10
#define ESTATE_SENSOR1 11
#define ESTATE_WIFI 12
#define ESTATE_MQTT 13
#define ESTATE_WIFI_PORTAL 20
int esknodoState = 0;
/**
 * If true, the system should publish its measurements
 * Only for devel mode
 */
bool publishRequest = false;


/**
 * Setup initialises the hardware.
 * After setup:
 * Battery should be checked.
 * Sleep cycle management is also be done in setup.
 * Everything else should depend on battery and sleep cycles are ok.
 * If so, Wifi setup should be started.
 * Mqtt server is connected in loop().
 */
void setup()
{
    esknodoState = ESTATE_STARTED;
    // "Remember to set your serial monitor to 74880 baud.
    // This odd speed will show ESP8266 boot diagnostics too"
    if (developOptions & DevSerialSpeed74880ForBootMsg) {
        Serial.begin(74880);
    }
    else {
        Serial.begin(115200);
    }
    Serial.println();
    Serial.println("Serial start");
    esknodoState = ESTATE_SERIAL1;

    // Battery Check
    Battery.loop();
    delay(1);
    if (Battery.getState() == ESPBATTERY_CRITICAL) {
        Serial.println("Battery is critical. Shutdown. V/%=" + String(Battery.getVoltage())+ "/" + String(Battery.getPercentage()));
        // wakeup only by manual reset
        SleepMgr.shutdownFinally();
        // blink not available blinkMessageAndTerminate(BLINKCODE_BATTERY_LOW, true);
    }
    else {
        Serial.println("Battery OK. " + String(Battery.getVoltage()) + "V = " + String(Battery.getPercentage())+"%");
    }
    esknodoState = ESTATE_BATTERY;

    // Serial line check
    while (!Serial.availableForWrite()) {
        if(millis() > 5000) {
            Serial.println("serial setup timeout");
            // blink not available   blinkMessageAndTerminate(BLINKCODE_SERIAL_FAILED, false);
            // wakeup only by manual reset
            SleepMgr.shutdownFinally();
        }
        yield();
    }
    esknodoState = ESTATE_SERIAL2;

    // Detect reset reason
    Serial.println("reset reason "+String(ESP.getResetReason()));

    // load develop options if set in EEStore
    // if no dev options are set in EEStore, the default values of the programm are used
    if (EEStore.data.developOptions) {
        developOptions = EEStore.data.developOptions;
        Serial.println("develop options loaded from eeprom=" + developOptionsToString(developOptions));
    }
    else {
        Serial.println("develop options from main=" + developOptionsToString(developOptions));
    }
    esknodoState = ESTATE_DEV_OPTIONS;

    // start tickers
    systemTicker1.attach_ms(1, tick1ms);
    systemTicker100.attach_ms(100, tick100ms);
    esknodoState = ESTATE_TICKERS;

    // setup button (experimental)
    setupButton();
    esknodoState = ESTATE_BUTTONS;

    // check sleep/wakeup status
    int wakeUpsSoFar = SleepMgr.manageWakeupCycles(true, 3000);
    if(esknodoState == ESTATE_WIFI_PORTAL) {
        Serial.println("PORTAL MODE");
        setupWifiSettings();
        WiFiSettings.portal();
        // wifi connect() and mqtt setup() must not be called:
        return;
    }
    esknodoState = ESTATE_SLEEP_CYCLES;

    // Sets power port pin to output.
    // This pin is attached to the enable pin of the sensors power regulator.
    // It also switches on the I2C bus power.
    setPeripheralBusPower(SENSOR_POWER_ON);
    esknodoState = ESTATE_SENSORS;

    delay(200);
    // set it to e.g. 10kHz
    Wire.setClock(10000);
    Wire.setClockStretchLimit(4000);
    esknodoState = ESTATE_I2C;

    // provisional AM2320 code
    Serial.println("Adafruit AM2320 Esknodo");
    // fix I2C-Address 0x5C
    am2320.begin();
    esknodoState = ESTATE_SENSOR1;

    // reset sensor
    // default address 0x20
    CatnipSensor.begin();

    // Will format on the first run after failing to mount
    setupWifiSettings();
    // Connect to WiFi with a timeout of ... seconds
    // Launches the portal if the connection failed
    Serial.println("mqtt server="+mqttServer);
    Serial.println("mqtt port="+String(mqttPort));

    // this execution branch is concurrent to wifi timeout (WIFI_UNCONNECTED_TIMEOUT)
    Serial.println("start wifi connect");
    WiFiSettings.connect(WIFI_PORTAL_AUTOMATIC, WIFI_PORTAL_WAIT_FOR_WIFI_SECONDS);
    esknodoState = ESTATE_WIFI;
    setupMqtt();
    esknodoState = ESTATE_MQTT;

    // give some time to boot up
    // Serial.print("I2C Soil Moisture Sensor Address: ");
    // Serial.print(CatnipSensor.getAddress(),HEX);
    // Serial.print(" Firmware ");
    // Serial.print(CatnipSensor.getVersion(), HEX);
    // Serial.println();

    Serial.println("end of main setup");
}

/**
 * Sets up klick of flash button
 */
void setupButton()
{
    static uint32_t longPressStart;
    // Events are attached with lambda functions
    FirstButton.attachClick([]() {
        Serial.println("Single Pressed!");
        if(SleepMgr.isInShutdown()) {
            SleepMgr.cancelShutdown();
        }
        publishRequest = true;
    });
    // Double Click
    FirstButton.attachDoubleClick([]() {
        Serial.println("Double Pressed: Reset of develop options");
        onClearDevelopOptions();
    });
    // Double and more Clicks
    FirstButton.attachMultiClick([]() {
        // this function will be called when the button was pressed multiple times in a short timeframe.
        int n = FirstButton.getNumberClicks();
        if (n == 2) {
            Serial.println("Multiclick 2: Reset of develop options");
            onClearDevelopOptions();
        }
        else if (n == 3) {
            if(SleepMgr.isInShutdown()) {
                esknodoState = ESTATE_WIFI_PORTAL;
                SleepMgr.cancelShutdown();
                Serial.println("tripleClick detected: Portal Mode");
            }
            else {
                Serial.println("tripleClick detected");
                EEStore.wipe();
            }
        }
        else if (n == 4) {
            Serial.println("quadrupleClick detected.");
        }
        else {
            Serial.printf("multiClick(%u) detected\r\n", n);
        }

    });
    FirstButton.attachLongPressStart([]() {
        Serial.println("long press start");
        longPressStart = millis();
    });
    FirstButton.attachLongPressStop([]() {
        Serial.println("long press stop");
        if(millis() - longPressStart > 4000) {
            // long press duration > 4sec
        }
    });
}

void onClearDevelopOptions()
{
    EEStore.data.developOptions = 0;
    developOptions = 0;
    Serial.println("develop options cleared");
    EEStore.commit();
}

/**
 * Sets the power supply of the sensors and I2C bus to the given state.
 */
void setPeripheralBusPower(bool powerState)
{
    // Sets power port pin to output.
    // This pin is attached to the enable pin of the sensors power regulator.
    // It also switches on the I2C bus power.
    pinMode(PORTPIN_SENSOR_SUPPLY, OUTPUT);
    digitalWrite(PORTPIN_SENSOR_SUPPLY, powerState);
}

/**
 * Starts setup of wifi.
 * Successful setup of wifi is messaged via global "wifiReady" variable.
 * Is non-blocking.
 */
void setupWifiSettings()
{
    LittleFS.begin();

    WiFiSettings.hostname = "Esknodo-";
    WiFiSettings.secure = true;

    // Set custom callback functions
    WiFiSettings.onSuccess  = []() {
        Serial.println("wifi manager success");
        wifiReady = true;
    };
    WiFiSettings.onFailure  = []() {
        // no connection to a wifi
        Serial.println("wifi manager fail");
        blinkMessageAndTerminate(BLINKCODE_WIFI_FAILED, false);
    };
    WiFiSettings.onWaitLoop = []() {
        LedSystem.setBlinking(1);
        Serial.println("wait for wifi manager setup");
         // Delay next function call by 500ms
        return 500;
    };
    WiFiSettings.onPortalWaitLoop = []() {
        // limit time and energy while in portal mode
        static unsigned long lastMillis;
        if (Battery.getState() == ESPBATTERY_CRITICAL || Battery.getState() == ESPBATTERY_LOW) {
            Serial.println("Battery is critical. Shutdown. V/%=" + String(Battery.getVoltage())+ "/" + String(Battery.getPercentage()));
            // wakeup only by manual reset
            blinkMessageAndTerminate(BLINKCODE_BATTERY_LOW, true);
        }
        else if(millis() > WIFI_PORTAL_ACTIVE_SECONDS*1000) {
            Serial.println("wifi portal timeout of "+String(WIFI_PORTAL_ACTIVE_SECONDS));
            // it makes no sense to wake up again and enter portal mode: nobody will be there to key in access data
            blinkMessageAndTerminate(BLINKCODE_BATTERY_LOW, true);
        }
        if(millis() > lastMillis + 8000) {
            Serial.println("wait for wifi manager portal user input, remaining: "+String(WIFI_PORTAL_ACTIVE_SECONDS - millis()/1000));
            lastMillis = millis();
        }
    };

    // Define custom settings saved by WifiSettings
    // These will return the default if nothing was set before
    // These functions should be called before calling .connect() or .portal().
    mqttServer = WiFiSettings.string("mqtt_server", 64, MQTT_SERVER_DEFAULT);
    mqttPort = WiFiSettings.integer("mqttPort", MQTT_PORT_DEFAULT);
    mqttClientID = WiFiSettings.string("mqtt_clientid", 32, MQTT_CLIENTID_DEFAULT);
    mqttUsername = WiFiSettings.string("mqtt_username", 32, MQTT_USERNAME_DEFAULT);
    mqttPassword = WiFiSettings.string("mqtt_password", 32, MQTT_PASSWORD_DEFAULT);
    mqttTopic = WiFiSettings.string("mqtt_topic", 64, MQTT_TOPIC_DEFAULT, "topic with trailing slash!");
    MqttConnection.QOS = WiFiSettings.integer("mqtt_qos", MQTT_QOS_DEFAULT);
    MqttConnection.cleanSession = WiFiSettings.checkbox("Mqtt clean session", MQTT_CLEAN_SESSION_DEFAULT);

}

/**
 * Waits for wifi, then sets up mqtt.
 * Is blocking.
 * @todo move wait to mqtt class, but timeout should remain here
 */
void setupMqtt()
{
    unsigned long lastMillis = 0;
    // counts seconds since last connection
    static unsigned int unconnectedTimer = 0;

    while (!wifiReady) {
        if(millis() - 1000 > lastMillis) {
            Serial.println("waiting for wifi setup");
            lastMillis = millis();
            unconnectedTimer ++;
        }
        if(unconnectedTimer > WIFI_UNCONNECTED_TIMEOUT) {
            // this execution branch is concurrent to portal mode activation (WIFI_PORTAL_WAIT_FOR_WIFI_SECONDS)
            Serial.println("wifi setup timeout");
            blinkMessageAndTerminate(BLINKCODE_WIFI_TIMEOUT, false);
        }
        yield();
    }

    if (wifiReady) {
        if (FirstButton.isLongPressed()) {
            // clears wifi, shows portal page
            Serial.println("button long press: entering portal");
            WiFiSettings.portal();
        }
        else {
            Serial.println("wifi ready");
        }
    }

    Serial.println("start of mqtt setup");
    unconnectedTimer = 0;
    MqttConnection.setup();
    MqttConnection.loop();
    mqttReady = MqttConnection.reconnect();
    MqttConnection.loop();
    while (!mqttReady) {
        mqttReady = MqttConnection.reconnect();
        if(millis() > lastMillis + 1000) {
            Serial.println("waiting for mqtt setup");
            lastMillis = millis();
            unconnectedTimer ++;
        }
        if(unconnectedTimer > MQTT_UNCONNECTED_TIMEOUT) {
            Serial.println("mqtt setup timeout");
            blinkMessageAndTerminate(BLINKCODE_NO_MQTT, false);
        }
        yield();
        MqttConnection.loop();
    }
}


/**
 * transmits status update.
 */
void loop()
{
    static unsigned long lastMillis, delayMarker;
    bool heartBeat = false;
    static bool firstCall = true;

    if(firstCall) {
        if(esknodoState == ESTATE_WIFI_PORTAL) {
            Serial.println("Portal Mode");
        }
        else {
            Serial.println("entering loop");
        }
        firstCall = false;
    }

    // "heartbeat"
    if(millis() > lastMillis + 12000) {
        lastMillis = millis();
        heartBeat = true;
    }

    if(heartBeat) {
        // check battery level in no sleep mode
        if (Battery.getState() == ESPBATTERY_CRITICAL) {
            Serial.println("Battery is critical. Shutdown. Percentage=" + String(Battery.getPercentage()));
            // wakeup only by manual reset
            blinkMessageAndTerminate(BLINKCODE_BATTERY_LOW, true);
        }

        if(esknodoState == ESTATE_WIFI_PORTAL) {
            // do nothing
        }
        else {
            // Soil moisture sensor
            int temp;
            Serial.print("main-loop heartbeat ");
            Serial.println(millis()/1000);
            Serial.println(Sensor0.getDebugInfo());
            Serial.println(Sensor1.getDebugInfo());
            Serial.println(Sensor2.getDebugInfo());
            // AM2320 readings
            Serial.print("AM2320 Temp: "); Serial.println(am2320.readTemperature());
            Serial.print("AM2320 Hum: "); Serial.println(am2320.readHumidity());

            // Catnip Sensor
            // delayMarker = millis()+4000;
            // while (CatnipSensor.isBusy()) { // available since FW 2.3
            //     delay(50);
            //     // max wait 4sec
            //     if(millis() > delayMarker) {
            //         break;
            //     }
            // }
            // if (CatnipSensor.isBusy()) {
            //     Serial.print("catnip sensor is busy");
            // }
            // else {
            //     Serial.print("Soil Moisture Capacitance: ");
            //     Serial.print(CatnipSensor.getCapacitance()); //read capacitance register
            //     Serial.print(", Temperature: ");
            //     Serial.print(CatnipSensor.getTemperature()/(float)10); //temperature register
            //     Serial.print(", Light: ");
            //     Serial.println(CatnipSensor.getLight(true)); //request light measurement, wait and read light register
            //     //CatnipSensor.sleep(); // available since FW 2.3
            // }
        }
    }

    if(esknodoState == ESTATE_WIFI_PORTAL) {
        // do nothing
    }
    else if(!Sensor0.isReady(1) || !Sensor1.isReady(1)) {
        if(heartBeat) {
            Serial.println("waiting for sensors");
        }
    }
    else if(!MqttConnection.isReallyReady()) {
        if(heartBeat) {
            Serial.println("waiting for mqtt");
        }
    }
    else {
        if (developOptions & DevNoSleep) {
            if(publishRequest) {
                // publish request from button click while nosleep
                MqttPublish(false);
                publishRequest = false;
            }
        }
        else if(publishRequest) {
            // publish request from button click during startup
            MqttPublish(true);
            SleepMgr.shutdown();
        }
        else if(SleepMgr.isActiveCycle()) {
            // publish request by active cycle
            if (developOptions & DevPublishConfig) {
                MqttPublish(true);
            }
            else {
                MqttPublish(false);
            }

            SleepMgr.shutdown();
        }
        else {
            // system was active for nothing
            SleepMgr.shutdown();
        }
    }

    yield();
}

/**
 * Publishes sensor values
 */
bool MqttPublish(bool withConfig)
{
    bool mqttSuccess = MqttConnection.publishMeasurements(Sensor0, Sensor1);
    yield();
    if(mqttSuccess && withConfig) {
        mqttSuccess = MqttConnection.publishConfig();
    }
    yield();
    //MoistureSensorTest1();
    yield();

    return mqttSuccess;
}

/**
 * @ignore experimental
 */
int scanWifi ()
{
    // WiFi.scanNetworks will return the number of networks found
    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0) {
        Serial.println("no networks found");
    }
    else {
        Serial.print(n);
        Serial.println(" networks found");
        for (int i = 0; i < n; ++i) {
            // Print SSID and RSSI for each network found
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
            delay(10);
        }
    }
    Serial.println("");
    return n;
}

/**
 * Sends final (most probably error-) blink code and shuts down.
 * This is usually used for setup errors.
 * If battery is critical, the command is rewritten to shutdownFinally.
 */
void blinkMessageAndTerminate(int messageCode, bool stopFinally)
{
    // if battery is low no wake-up should be planned
    if (!stopFinally && Battery.getState() == ESPBATTERY_CRITICAL) {
        Serial.println("Battery is critical. Shutdown. V/%=" + String(Battery.getVoltage())+ "/" + String(Battery.getPercentage()));
        // rewrite messge code to wakeup only by manual reset
        messageCode = BLINKCODE_BATTERY_LOW;
        stopFinally = true;
    }

    // execute blink code and stop command
    LedSystem.setBlinkCode(messageCode);
    while (true)
    {
        if(LedSystem.isReady()) {
            if(stopFinally) {
                SleepMgr.shutdownFinally();
            }
            else {
                SleepMgr.shutdown();
            }
        }
        yield();
    }
}


void tick1ms()
{
    Sensor0.tick1ms();
    Sensor1.tick1ms();

    // some loop functions are moved here, in order not to depend on the loop() function
    MqttConnection.loop();
    FirstButton.tick();
}


void tick100ms()
{
  LedSystem.routine100ms();
}

/**
 * Returns readable string of developer options.
 * The keys used here should be identical to the ones
 * used in the "devel" topic.
 * @todo move to a develop-option class
 */
String developOptionsToString(int developOptions)
{
    return String(
            String((developOptions&DevOverrideOptions)?"override ":"")+
            String((developOptions&DevSerialSpeed74880ForBootMsg)?"serialBootMsg ":"")+
            String((developOptions&DevPublishConfig)?"publishCfg ":"")+
            String((developOptions&DevNoSleep)?"noSleep ":"")+
            String((developOptions&DevShortSleep)?"shortSleep ":"")+
            String((developOptions&DevShowConfidentials)?"showConfident ":"")+
            String((developOptions&DevMoreVerbose)?"verbose ":"")+
            String((developOptions&DevSensorVerbose)?"sensVerbose":"")
    );
}

/**
 * This might be very poor software design
 * @todo move to a develop-option class
 */
void executeBoolExpressionOnDevelOption(int boolExpression, int develOption)
{
    if(boolExpression == 1) {
        developOptions |=  develOption;
    }
    else {
        developOptions &= ~develOption;
    }
}


/**
 * Tests calculation
 */
void MoistureSensorTest1()
{
    int raw;
    int normalized;
    Serial.println("MoistureSensorTest1");
    MoistureSensor1 TestSensor = MoistureSensor1();
    TestSensor.setMoistureMaxConfig(1500);
    TestSensor.setMoistureMinConfig(2000);

    Serial.println("raw -> normalized");
    raw = 1000;
    while ( raw <= 3000) {
        normalized = TestSensor.rawToNormalized(raw);
        Serial.printf("%4u -> %4u\r\n", raw, normalized);
        raw += 400;
        yield();
    }
    Serial.println("raw -> normalized (pass2)");
    raw = 1000;
    while ( raw <= 3000) {
        normalized = TestSensor.rawToNormalized(raw);
        Serial.printf("%4u -> %4u\r\n", raw, normalized);
        raw += 400;
        yield();
    }
}

