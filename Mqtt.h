/**
 * Represents the MQTT connection of the esknodo sensor.
 * This is specific for esknodo.
 */

#ifndef __MQTT_H__
#define __MQTT_H__

#include "MoistureSensor1.h"
#include "esknodo.h"
#include <Arduino.h>

#include <ArduinoJson.h>

#define MQTT_CONFIG_MAX_MOISTURE_LEVEL  "maxMoistCalib"
#define MQTT_CONFIG_MIN_MOISTURE_LEVEL  "minMoistCalib"
#define MQTT_CONFIG_MOISTURE_LEVELS     "moistCalib"
#define MQTT_CONFIG_CLOCK_ADJUST        "clockAdjust"

#define MQTT_CONFIG_MOISTURE_SENSOR_1   "FS1"
#define MQTT_CONFIG_MOISTURE_SENSOR_2   "FS2"
#define MQTT_CONFIG_MOISTURE_SENSOR     "MS-"

#define MQTT_CONFIG_MIN_MOIST_1         "minMoist1"
#define MQTT_CONFIG_MAX_MOIST_1         "maxMoist1"
#define MQTT_CONFIG_MIN_MOIST_2         "minMoist2"
#define MQTT_CONFIG_MAX_MOIST_2         "maxMoist2"

#define MQTT_CONFIG_SLEEP_CYCLES        "sleepCycles"
#define MQTT_BATTERY_SLEEP_CYCLES       "battSleepC"
// developer options
#define MQTT_CONFIG_DEVEL_SHORT_SLEEP        "devShSlp"
#define MQTT_CONFIG_DEVEL_OVERRIDE_OPTIONS   "devOvrOpt"


class Mqtt {
private:
    /**
     * Holds millisecond-timestamp when mqtt made connection.
     */
    unsigned long connectedAt = 0;
    String errorCode2Text(int code);
    int parseBoolExpression(String PayloadString);
    bool subscribe();
    const char * generateID();

public:
    int QOS = 0;
    bool cleanSession = true;
    Mqtt();
    void setup();
    bool loop();
    bool isReady(); // unused
    bool isReallyReady();
    bool reconnect();
    bool publishMeasurements(MoistureSensor1 Sensor0, MoistureSensor1 Sensor1);
    bool publishConfig();
    bool publishTopic(String topic, char * message);
    /**
     * @todo this should be moved to an devel-options class
     */
    String developOptionsToString(int developOptions);
    void callback(char* topic, byte* payload, unsigned int length);
    void parseSensorCalibration(String TopicString, String PayloadString);
    void parseConfigMessage(DynamicJsonDocument PayLoadDocument);
    void parseDevelMessage(String payload);
    String getDebugInfo();
};


#endif
