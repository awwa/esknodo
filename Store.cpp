/**
 * Eeprom store of esknodo.
 * This is mostyl copy/paste from the library examples.
 *
 * Examples:
 * https://github.com/jwrw/ESP_EEPROM/blob/master/examples/
 */

#include "Store.h"
#include <ESP_EEPROM.h>
#include <limits.h>
#include <Arduino.h>


Store::Store()
{
    // The begin() call will find the data previously saved in EEPROM if the same size
    // as was previously committed. If the size is different then the EEEPROM data is cleared.
    // Note that this is not made permanent until you call commit();
    this->setup(sizeof(EEPROMStruct));
}


void Store::setup(unsigned int eepromSize)
{
    // The begin() call will find the data previously saved in EEPROM if the same size
    // as was previously committed. If the size is different then the EEEPROM data is cleared.
    // Note that this is not made permanent until you call commit();
    EEPROM.begin(eepromSize);

    // Check if the EEPROM contains valid data from another run
    // If so, overwrite the 'default' values set up in our struct
    if(EEPROM.percentUsed() >= 0) {
        EEPROM.get(0, this->data);
        Serial.println("EEPROM has data from a previous run.");
        Serial.print(EEPROM.percentUsed());
        Serial.println("% of ESP flash space currently used");
    } else {
        Serial.println("EEPROM size changed - EEPROM data zeroed - commit() to make permanent");
    }
}

/**
 * commits volatile data
 */
bool Store::shutdown()
{
    // commits the store
    return this->commit();
}

/**
 * writes data to eeprom
 */
bool Store::commit()
{
    // set the EEPROM data ready for writing
    EEPROM.put(0, this->data);

    // write the data to EEPROM
    boolean ok = EEPROM.commit();
    Serial.println((ok) ? "EEPROM Commit OK" : "EEPROM Commit failed");
    return ok;
}

/**
 * Wipes data from eeprom
 */
bool Store::wipe()
{
    boolean result = EEPROM.wipe();
    if (result) {
        Serial.println("All EEPROM data wiped");
    }
    else {
        Serial.println("EEPROM data could not be wiped from flash store");
    }
    return result;
}
