
#ifndef __FREQUENCY_SIGNAL_H__
#define __FREQUENCY_SIGNAL_H__

class FrequencySignal {
private:
    int rawToNormalized(int raw);
    bool memoPinState;
    int portPin;
    unsigned int ticksToMeasure = 24000;
    unsigned int elapsedTicks = 0;
    unsigned int countedLevelTransitions = 0;
    /**
     * Holds the raw value of the last measurement
     */
    unsigned int raw = 0;
    unsigned int measurementsCounter = 0;

public:
    FrequencySignal();
    FrequencySignal(int portPin);
    void setup(int portPin);
    int getPortPin();
    void tick1ms();
    int getRawValue();
    unsigned int getMeasurementCounter();
};

#endif
