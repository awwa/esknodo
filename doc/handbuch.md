
# Esknodo Handbuch

# Bedeutung der LED
## Blink-Codes


# Tasten
## Reset-Taste
Die Reset Taste ist immer wirksam.

## Flash-Taste
Bei der Flash-Taste hängt die Funktion von der Zeit nach einem Reset ab.
Wird sie während eines Resets gedrückt, startet das System nicht.

### Innerhalb vier Sekunden nach einem Reset:

* Einfacher Klick
sendet eine Messung und Konfiguration, dann shutdown

* Doppelklick
löscht die Develop Flags

* Dreifachklick
aktiviert den Portal Modus

### Ab vier Sekunden nach einem Reset (noSleep Mode):

* Einfacher Klick
sendet die Messungen und Konfiguration

* Doppelklick
löscht die Develop Flags

* Dreifachklick
Löscht den den Konfigurations - Speicher


# Modi
* nosleep
* override
* shortSleep

# Config
config ist ein JSON Objekt

sleepCycles
Legt die Schlafzyklen fest

Ein Wert von 0 deaktiviert diese Option.

Ein Wert von 1 bedeuted: keine extra Schlafzyklen, bei jedem Aufwecken wird das Programm ausgeführt.

Schlafzyklen meint hier eine Aufwecken des Systems, wobei das System unmittelbar wieder in den Schlafmodus geht.

# Anschlüsse (außen)

## Sensoren
### Frequenz (Bodenfeuchte)
Manche Prozentwerte haben eine besondere Bedeutung:

0 - kein Signal vom Sensor

1,2 - Messbereich unterschritten

99,100 - Messbereich überschritten

### Kalibrierung der beiden Sensoren
Über MQTT:

MQTT Nachrichten bestehen aus Topic und Payload

Beispiel:

Topic-Präfix: garten/pflanze1/

Topic-Postfix Sensor1, Trockenwert: calib/minMoist1

gesamt-Topic: garten/pflanze1/calib/minMoist1

Payload ist der Kalibrierungswert, z.B. 5400


### I2C Temperatur, Luftfeuchte

### I2C Bodenfeuchte, Bodentemperatur


## Stromversorgung

### Akku

### Solarzelle

### Laden über USB


## Konfiguration per MQTT

### Beispiel Topic:
stuttgart/beet/gurken/

### Beispiele für Payloads:
publishCfg=true

shortSleep=false

### Liste der Json Schlüssel:
noSleep

override

serialBootMsg

shortSleep

showConfident

publishCfg

