/**
 * Pinout:
 *
 * 1/sw - GND
 * 3/rt - Vcc
 * 4/or - moisture signal
 *
 *
 */

#ifndef __SENSOR1_H__
#define __SENSOR1_H__

#include <inttypes.h>
#include <Arduino.h>

#include "install/hardware.h"
#include "FrequencySignal.h"

#define DEFAULT_MAX_MOISTURE 1000
#define DEFAULT_MIN_MOISTURE 10000
#define ERR_MAX_BORDER_IS_HIGHER_THAN_MIN_BORDER -1

#include "MoistureSensor12.h"


class MoistureSensor1 {
private:
    /**
     * Holds sensor ID to access sensor config etc.
     */
    byte sensorId;
    /**
     * Sets power on or off
     */
    void setPower(bool powerState);
    /**
     * Represents a measurable frequency which measures a physical value.
     */
    FrequencySignal Moisture;

public:
    MoistureSensor1();
    MoistureSensor1(int portPin, byte sensorId);
    void tick1ms();
    void setup(int PortPin);
    byte getSensorId();
    /**
     * The value for maximum (100%, wet) moisture.
     * It can be adjusted via mqtt and is stored in eeprom
     */
    int maximumMoistureRaw = DEFAULT_MAX_MOISTURE;
    /**
     * The value for minimum (0% moisture, dry) condition.
     * Setting the "dry" value to the "free air" value would result in poor sensitivity of the sensor.
     * Therefore, the minimum moisture value should be tested with dry soil.
     */
    int minimumMoistureRaw = DEFAULT_MIN_MOISTURE;

    void updateFromEEStore();
    void setDefaultCalibrationValues();

    int getRawMoisture();
    int rawToNormalized(int raw);
    int getMoisture();

    void setMoistureMaxConfig(int maxValue);
    int getMoistureMaxConfig();
    void setMoistureMinConfig(int minValue);
    int getMoistureMinConfig();

    bool isReady(int measurementsCount);
    String getDebugInfo();
};

#endif
