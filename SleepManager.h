/**
 * Organises sleep time, wakeup cycles and shutdown
 *
 */

#ifndef __SLEEPMANAGER_H__
#define __SLEEPMANAGER_H__

#include <Arduino.h>

/**
 * Sleep Cycles
 * The system wakes up every 60 or 3600 seconds and counts up the slept cycles.
 * After the amount of slept cycles noted below, it remains
 * active and executes the program.
 * By that the sleep cycles control the waited time between activity of the system.
 * The value is increased with weaker battery.
 */
class SleepManager {
    private:
        /**
         * sleep time in seconds (minutes*seconds)
         * The value is managed by the switch "DevShortSleep"
         * (develop: 1min , production: 60min)
         */
        uint32_t sleepTimeSeconds = 60*60;
        /**
         * Gets sleep seconds
         */
        uint32_t getSleepTimeSeconds();
        /**
         * sleepCycles calculated from e.g. battery level
         * the lower the battery level, the more cycles are slept between active cycles.
         */
        int calculatedSleepCycles;
        /**
         * shuts down the system for the given time in seconds
         */
        void shutdown(uint32_t sleepTime);
        /**
         * If this flag is set, the shutdown has been requested, but has also a delay
         */
        bool isInShutdownFlag = false;
        /**
         * If this flag is set, the shutdown is cancelled
         */
        bool cancelShutdownFlag = false;
        /**
         * adjust clock
         */
        unsigned long adjustSleepSeconds(unsigned long microseconds);

    public:
        SleepManager();
        int manageWakeupCycles(bool shutdown);
        int manageWakeupCycles(bool shutdown, uint16_t delay);
        bool isActiveCycle();
        int getCalculatedSleepCycles();
        int batteryPercentToSleepCycles(int batteryPercent);
        void shutdown();
        void shutdownFinally();
        bool isInShutdown();
        void cancelShutdown();
};

#endif
