/**
 * Represents the MQTT connection of the esknodo sensor.
 * This is specific for esknodo.
 *
 * $.config contains a JSON document. It keys are:
 *
 * maxMoistCalib
 * minMoistCalib
 *
 * and others.
 *
 * @see https://github.com/LennartHennigs/ESPBattery
 *
 * @see https://arduinojson.org/v6/api/dynamicjsondocument/
 */

#include <limits.h>
#include <Arduino.h>

#include "SleepManager.h"
#include "Mqtt.h"
#include "esknodo.h"
#include "install/hardware.h"
#include "Store.h"
#include "MoistureSensor1.h"

#include "Adafruit_AM2320.h"

#include <ArduinoJson.h>
#include <ESP_EEPROM.h>
#include <ESPBattery.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

extern int developOptions;
extern String mqttServer;
extern uint16_t mqttPort;
extern String mqttClientID;
extern String mqttUsername;
extern String mqttPassword;
extern String mqttTopic;

// const char* mqtt_username = mqttUsername.c_str();
// const char* mqtt_password = mqttPassword.c_str();
// const char* mqtt_clientID = mqttClientID.c_str();
// const char* mqtt_topic = mqttTopic.c_str();

WiFiClient espClient;
PubSubClient MqttClient(espClient);
extern Mqtt MqttConnection;

/**
 * Permanent EEPROM store
 * Usage: Store.data.VARNAME
 */
extern Store EEStore;
/**
 * Needed to show sleep cycles
 */
extern SleepManager SleepMgr;

// mqtt callback is global
void mqttCallbackFunction(char* topic, byte* payload, unsigned int length);

// we have 2 sensors of type 1 attached
extern MoistureSensor1 Sensor0;
extern MoistureSensor1 Sensor1;
extern MoistureSensor1 Sensor2;

extern Adafruit_AM2320 am2320;

extern ESPBattery Battery;


/**
 * Mqtt class
 * (required)
 */
Mqtt::Mqtt()
{
}

/**
 * Sets remote mqtt server and subscription callback.
 */
void Mqtt::setup()
{
    Serial.println("Setup Mqtt ClientID="+String(mqttClientID));
    // Serial.println("Setup Mqtt Server="+String(mqtt_server));
    Serial.println("Setup Mqtt Port="+String(mqttPort));

    this->connectedAt = 0;
    // setup mqtt
    MqttClient.setServer(mqttServer.c_str(), mqttPort);
    MqttClient.setCallback(mqttCallbackFunction);
}

/**
 * Returns true if mqtt is ready
 * @return true
 * @return false
 */
bool Mqtt::isReady()
{
    if (this->reconnect()) {
        if (!this->connectedAt) {
            this->connectedAt = millis();
        }
        return true;
    }
    else {
        return false;
    }
}

/**
 * Returns true if mqtt is really ready (mqtt connect + 3seconds)
 * @return bool
 */
bool Mqtt::isReallyReady()
{
    if (this->reconnect()) {
        if (millis() > this->connectedAt + 5000) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * MQTT connect and subscribe to config and devel topic items.
 * Should be called until it returns TRUE.
 * To receive subscribed messages when sleeping:
 * - The MqttClient-ID must not change
 * - Session must persist. (not be cleaned)
 * - The qos of publishing and subscribing must be >= 1
 * - It is not useful to "retain" the message
 * @return bool
 */
bool Mqtt::reconnect()
{
    static unsigned long lastCall;
    unsigned long now = millis();
    bool connected = false;

    if (MqttClient.connected()) {
        if (!this->connectedAt) {
            this->connectedAt = millis();
        }
        return true;
    }
    else {
        // not connected
        if (now - lastCall < 0) {
            // ticker overflew, only update lastTicker value
            // without further correction, this will -worst case- double the time interval
            lastCall = now;
            return false;
        }
        else if (now - lastCall > 5000) {
            // minimum wait time is 5000ms
            lastCall = now;
            Serial.print("Attempting MQTT connection, MqttClient: ");

            if(mqttClientID.equals("random")) {
                mqttClientID = String(this->generateID());
                Serial.print("generated random client-id="+mqttClientID);
            }

            // simple connect does not allow to receive subscribed messages
            connected = MqttClient.connect(mqttClientID.c_str(), mqttUsername.c_str(), mqttPassword.c_str(),
                        "lastWillTopic", MQTTQOS0, false, "lastWillMessage",
                        this->cleanSession);
            Serial.println(this->getDebugInfo());

            if (connected) {
                Serial.print("Mqtt connect success, state=");
                Serial.println(MqttClient.state());
                bool success = this->subscribe();
                this->connectedAt = millis();
                return true;
            }
            else {
                this->connectedAt = 0;
                Serial.print("Mqtt connect failed, state=");
                Serial.println(MqttClient.state());
                Serial.println("wait to retry");
                return false;
            }
        }
    }
    return false;
}

/**
 * @see https://beebotte.com/tutorials/
 */
const char * Mqtt::generateID()
{
    const char chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    char randomId[17];

    randomSeed(analogRead(0));
    int i = 0;
    for(i = 0; i < sizeof(randomId) - 1; i++) {
        randomId[i] = chars[random(sizeof(chars))];
    }
    randomId[sizeof(randomId) -1] = '\0';

    return randomId;
}


bool Mqtt::subscribe()
{
    bool success = true;
    Serial.println("Start of subscribing");

    String interestingTopic[] = {MQTT_TOPIC_POSTFIX_CONFIG, MQTT_TOPIC_POSTFIX_DEVEL, MQTT_TOPIC_CALIBRATION_SUBSCRIPTION};
    int count_of_topics = 3;

    for (int i = 0; i < count_of_topics; i++) {
        if (success) {
            // assemble base topic and interesting subtopic
            String currentTopic = mqttTopic;
            currentTopic.concat(interestingTopic[i]);
            // maximum subscribe qos is "1"
            success = MqttClient.subscribe(currentTopic.c_str(), this->QOS);
            if (success) {
                Serial.print("mqtt subscribed to: ");
            } else {
                Serial.print("mqtt subscription failed for: ");
            }
            Serial.println(currentTopic);
            MqttClient.loop();
        }
    }

    return success;
}

/**
 * Publish sensor measurements
 */
bool Mqtt::publishMeasurements(MoistureSensor1 Sensor0, MoistureSensor1 Sensor1)
{
    char msg[512];

    bool connected = this->reconnect();
    if(!connected) {
        Serial.println("MQTT not connected: Error");
        Serial.println(this->getDebugInfo());
        return false;
    }

    // assembling topic
    String topic = mqttTopic;
    topic.concat(MQTT_TOPIC_POSTFIX_SENSOR);

    // assembling payload
    // looks like there is an undocumented limit of JSON string length of maybe 256
    /// @see https://arduinojson.org/v6/assistant/
    DynamicJsonDocument jsonDoc(512);

    jsonDoc["battV"] = Battery.getVoltage();
    jsonDoc["battP"] = Battery.getPercentage();

    jsonDoc["s1raw"] = Sensor0.getRawMoisture();
    jsonDoc["sensor1"] = Sensor0.getMoisture();
    jsonDoc["s2raw"] = Sensor1.getRawMoisture();
    jsonDoc["sensor2"] = Sensor1.getMoisture();

    jsonDoc["hum"] = am2320.readHumidity();
    jsonDoc["temp"] = am2320.readTemperature();

    // show develop options
    jsonDoc["dev"] = this->developOptionsToString(developOptions);
    serializeJson(jsonDoc, msg);

    return publishTopic(topic.c_str(), msg);
}


/**
 * Publishes configurable values.
 * Devel options are published too, but they should merge with /config in the future
 */
bool Mqtt::publishConfig()
{
    char msg[256];
    char develBuffer[256];

    bool connected = this->reconnect();
    if(!connected) {
        Serial.println("MQTT not connected: Error");
        Serial.println(this->getDebugInfo());
        return false;
    }

    // assembling config topic
    String topic = mqttTopic;
    topic.concat(MQTT_TOPIC_POSTFIX_CONFIG);

    // assembling payload
    // looks like there is an undocumented limit of JSON string length of maybe 256
    /// @see https://arduinojson.org/v6/assistant/
    DynamicJsonDocument jsonDoc(256);
    // Moisture Sensor
    // moisture-%, -raw, min-, max-config
    char buffer[64];
    sprintf(buffer, "%u%% (%u) [%u,%u]", Sensor0.getMoisture(), Sensor0.getRawMoisture(), Sensor0.getMoistureMinConfig(), Sensor0.getMoistureMaxConfig() );
    jsonDoc[MQTT_CONFIG_MOISTURE_SENSOR+String(Sensor0.getSensorId())] = buffer;
    sprintf(buffer, "%u%% (%u) [%u,%u]", Sensor1.getMoisture(), Sensor1.getRawMoisture(), Sensor1.getMoistureMinConfig(), Sensor1.getMoistureMaxConfig() );
    jsonDoc[MQTT_CONFIG_MOISTURE_SENSOR+String(Sensor1.getSensorId())] = buffer;
    // Sleep Mgmt
    jsonDoc[MQTT_CONFIG_SLEEP_CYCLES] = EEStore.data.sleepCycles;
    jsonDoc[MQTT_BATTERY_SLEEP_CYCLES] = SleepMgr.getCalculatedSleepCycles();
    // System
    jsonDoc["free"] = ESP.getMaxFreeBlockSize();
    jsonDoc["heap"] = ESP.getHeapFragmentation();
    // Develop options
    jsonDoc["dev"] = this->developOptionsToString(developOptions);
    serializeJson(jsonDoc, msg);

    return publishTopic(topic.c_str(), msg);
}


bool Mqtt::publishTopic(String topic, char * message)
{
    Serial.printf("Publish %s", topic.c_str());
    Serial.println(message);
    bool success = MqttClient.publish(topic.c_str(), message);
    Serial.println(success?"succeeded":"failed");
    return success;
}

/**
 * Returns readable string of developer options.
 * The keys used here should be identical to the ones
 * used in the "devel" topic.
 * @deprecated this should be moved to an devel-options class, see ino-file
 */
String Mqtt::developOptionsToString(int developOptions)
{
    return String(
            String((developOptions&DevOverrideOptions)?"override ":"")+
            String((developOptions&DevSerialSpeed74880ForBootMsg)?"serialBootMsg ":"")+
            String((developOptions&DevNoSleep)?"noSleep ":"")+
            String((developOptions&DevShortSleep)?"shortSleep ":"")+
            String((developOptions&DevShowConfidentials)?"showConfident ":"")+
            String((developOptions&DevMoreVerbose)?"verbose ":"")+
            String((developOptions&DevSensorVerbose)?"sensVerbose":"")
    );
}

/**
 * Gets develop info
 */
String Mqtt::getDebugInfo()
{
    String essential;
    String confidential;

    ///@todo
    String SytemInfo = String(String(PROJECT_NAME) + String("-") + String(ESP.getChipId()) );

    if (developOptions & DevShowConfidentials) {
        essential = String("SRV="+mqttServer+" PORT="+String(mqttPort)+" ClientID="+mqttClientID);
    }
    else {
        essential = String( printf(" SRV:%2d PORT:%u CLIENT:%2d", String(mqttServer).length(), (int)mqttPort/1000 ,  String(mqttClientID).length() ));
    }
    essential += String(" QOS=" + String(this->QOS) + " CleanSession=" + String(this->cleanSession?"yes":"no"));

    if (developOptions & DevShowConfidentials) {
        confidential = String(" USER="+String(mqttUsername)+" PW="+String(mqttPassword));
    }
    else {
        confidential = String(mqttUsername).length()?" has-username":"no-username" + String(mqttUsername).length()?" has-password":"no-password";
    }
    return essential + confidential;
}


bool Mqtt::loop()
{
    return MqttClient.loop();
}


/**
 * Receives messages from subscriptions.
 *
 * @param topic
 * @param payload
 * @param length
 */
void mqttCallbackFunction(char* topic, byte* payload, unsigned int length)
{
    String TopicString = String(topic);
    Serial.println("Incoming Message, Topic="+TopicString);

    String PayloadString = String();
    for (int i = 0; i < length; i++) {
        PayloadString += (char)payload[i];
    }
    Serial.println(PayloadString);

    // config topic
    String topicConfig = String(mqttTopic);
    topicConfig.concat(MQTT_TOPIC_POSTFIX_CONFIG);
    // devel topic
    String topicDevel = String(mqttTopic);
    topicDevel.concat(MQTT_TOPIC_POSTFIX_DEVEL);
    // calibration topic
    String topicCalibration = String(mqttTopic);
    topicCalibration.concat(MQTT_TOPIC_CALIBRATION_QUERY);

    if (TopicString.equals(topicConfig)) {
        // from here on: configuration settings
        // config payload is expected to be json
        DynamicJsonDocument PayLoadDocument(1024);
        DeserializationError error = deserializeJson(PayLoadDocument, payload);
        if (error) {
            Serial.print("payload de-serial error: ");
            Serial.println(error.c_str());
            return;
        }
        MqttConnection.parseConfigMessage(PayLoadDocument);
    }
    /** @todo topic "devel" should be protected */
    else if (TopicString.equals(topicDevel)) {
        // from here on: develop options
        MqttConnection.parseDevelMessage(PayloadString);
    }
    else if (TopicString.startsWith(topicCalibration) ) {
        MqttConnection.parseSensorCalibration(TopicString, PayloadString);
    }
    else {
        Serial.print("Error: Unknown topic: ");
        Serial.println(topic);
    }

    // in no-sleep mode, shutdown is not called and eeprom is not committed
    if (developOptions & DevNoSleep) {
        EEStore.commit();
    }
}

/**
 * Parses sensor dry and wet calibration.
 */
void Mqtt::parseSensorCalibration(String TopicString, String PayloadString)
{
    // dry calibration
    int temp = PayloadString.toInt();
    if(TopicString.endsWith(MQTT_CONFIG_MAX_MOIST_1)) {
        Sensor0.setMoistureMaxConfig(temp);
        Serial.println("max moist 1");
    }
    // wet calibration
    else if(TopicString.endsWith(MQTT_CONFIG_MIN_MOIST_1)) {
        Sensor0.setMoistureMinConfig(temp);
        Serial.println("min moist 1");
    }
    // dry calibration
    else if(TopicString.endsWith(MQTT_CONFIG_MAX_MOIST_2)) {
        Sensor1.setMoistureMaxConfig(temp);
        Serial.println("max moist 2");
    }
    // wet calibration
    else if(TopicString.endsWith(MQTT_CONFIG_MIN_MOIST_2)) {
        Sensor1.setMoistureMinConfig(temp);
        Serial.println("min moist 2");
    }
    // clock calibration
    else if(TopicString.endsWith(MQTT_CONFIG_CLOCK_ADJUST)) {
        EEStore.data.clockAdjustment = temp;
        Serial.println("clock adjust");
    }
    else {
        Serial.println("Error: Unknown "+TopicString);
    }
}

/**
 * Parses fix sleep cycle, override option and short sleep.
 * from here on, the components of json payload are evaluated
 */
void Mqtt::parseConfigMessage(DynamicJsonDocument PayLoadDocument)
{
    // fixed sleep cycles
    if(PayLoadDocument.containsKey(MQTT_CONFIG_SLEEP_CYCLES)) {
        // Update Sleep Cycles
        Serial.print("programmed sleep cycles before: ");
        Serial.println(EEStore.data.sleepCycles);
        // new value
        int temp = (int)PayLoadDocument[MQTT_CONFIG_SLEEP_CYCLES];
        EEStore.data.sleepCycles = temp;
        // a value of "0" activates sleep cycle calculation from battery level
        if (temp == 0) {
            Serial.println("Value=0, => Sleep cycles are derived from battery level");
        }
        else {
            Serial.print("programmed sleep cycles after: ");
            Serial.println(EEStore.data.sleepCycles);
        }
    }
    // option override
    if(PayLoadDocument.containsKey(MQTT_CONFIG_DEVEL_OVERRIDE_OPTIONS)) {
        // override options
        Serial.print("EEStore developOptions before: ");
        Serial.println(EEStore.data.developOptions);
        bool incoming = (bool)PayLoadDocument[MQTT_CONFIG_DEVEL_OVERRIDE_OPTIONS];
        Serial.print("incoming override option=");
        Serial.println(incoming);
        if (incoming) {
            EEStore.data.developOptions |= DevOverrideOptions;
        }
        else {
            EEStore.data.developOptions &= ~DevOverrideOptions;
        }
    }
    // short sleep
    if(PayLoadDocument.containsKey(MQTT_CONFIG_DEVEL_SHORT_SLEEP)) {
        // override options
        Serial.print("EEStore developOptions before: ");
        Serial.println(EEStore.data.developOptions);
        bool incoming = (bool)PayLoadDocument[MQTT_CONFIG_DEVEL_SHORT_SLEEP];
        Serial.print("incoming short sleeep option=");
        Serial.println(incoming);
        if (incoming) {
            EEStore.data.developOptions |= DevShortSleep;
        }
        else {
            EEStore.data.developOptions &= ~DevShortSleep;
        }
    }
}

/**
 * Parses devel options
 */
void Mqtt::parseDevelMessage( String PayloadString)
{
    // interpretation as "key=value"
    Serial.println("topic develop options=" + developOptionsToString(developOptions));

    // bool can be 0=FALSE, 1=TRUE, -1 (default), -3 (invalid), -2 (unknown)
    int boolExpression = this->parseBoolExpression(PayloadString);

    if (boolExpression < 0) {
        Serial.println("unknown boolean representation at "+PayloadString);
    }
/*
    else if (PayloadString.indexOf("noSleep") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevNoSleep);
        Serial.println(PayloadString);
    }
*/
    else if (PayloadString.indexOf("override") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevOverrideOptions);
        Serial.println(PayloadString);
    }
    else if (PayloadString.indexOf("serialBootMsg") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevSerialSpeed74880ForBootMsg);
        Serial.println(PayloadString);
    }
    else if (PayloadString.indexOf("shortSleep") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevShortSleep);
        Serial.println(PayloadString);
    }
    else if (PayloadString.indexOf("showConfident") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevShowConfidentials);
        Serial.println(PayloadString);
    }
    else if (PayloadString.indexOf("publishCfg") == 0) {
        executeBoolExpressionOnDevelOption(boolExpression, DevPublishConfig);
        Serial.println(PayloadString);
    }
    else {
        Serial.println("unknown config command "+PayloadString);
    }

    Serial.println("topic develop options=" + developOptionsToString(developOptions));
    EEStore.data.developOptions = developOptions;
}

/**
 * Returns  0=FALSE, 1=TRUE, -1 (default), -3 (invalid), -2 (unknown)
 */
int Mqtt::parseBoolExpression(String PayloadString)
{
    // bool can be 0=FALSE, 1=TRUE, -1 (default), -3 (invalid), -2 (unknown)
    int boolExpression = -1;
    if (PayloadString.indexOf("=") == -1) {
        // no key/value
        boolExpression = -3;
    }
    else if (PayloadString.indexOf("=TRUE") != -1 || PayloadString.indexOf("=true") != -1 || PayloadString.indexOf("=on") != -1 || PayloadString.indexOf("=1") != -1) {
        // logical ON
        boolExpression = 1;
    }
    else if (PayloadString.indexOf("=FALSE") != -1 || PayloadString.indexOf("=false") != -1 || PayloadString.indexOf("=off") != -1 || PayloadString.indexOf("=0") != -1) {
        // logical OFF
        boolExpression = 0;
    }
    else {
        // unknown boolean representation
        boolExpression = -2;
    }
    return boolExpression;
}

/**
 * Gets error text from code
 */
String Mqtt::errorCode2Text(int code)
{
    String text;

    if (code == MQTT_CONNECTION_TIMEOUT) {
        text = "MQTT_CONNECTION_TIMEOUT";
    }
    else {
        text = "unknown error";
    }

// todo: plainttext error msg
// // Possible values for client.state()
// #define MQTT_CONNECTION_TIMEOUT     -4
// #define MQTT_CONNECTION_LOST        -3
// #define MQTT_CONNECT_FAILED         -2
// #define MQTT_DISCONNECTED           -1
// #define MQTT_CONNECTED               0
// #define MQTT_CONNECT_BAD_PROTOCOL    1
// #define MQTT_CONNECT_BAD_CLIENT_ID   2
// #define MQTT_CONNECT_UNAVAILABLE     3
// #define MQTT_CONNECT_BAD_CREDENTIALS 4
// #define MQTT_CONNECT_UNAUTHORIZED    5

    return text;
}
