/**
 * defines common moisture sensor constants
 */

#ifndef __SENSOR12_H__
#define __SENSOR12_H__

/** it is to dry, it is out ouf range */
#define ERR_MOISTURE_IS_OVERFLOW 100
#define ERR_MOISTURE_IS_NEGATIVE 2
#define ERR_RAW_VALUE_IS_ZERO 0
#define ERR_MIN_AND_MAX_CONFIG_ARE_EQUAL         -1

/** step size in percent in case of auto adaption to more wet measurement values than expected */
#define MAX_MOIST_AUTO_CALIB_STEPUP 10

#define MIN_MOISTURE_INDEX 0
#define MAX_MOISTURE_INDEX 1

#endif
