/**
 * Contains eeprom data specific for esknodo.
 * Usage: Store.Data.VARNAME
 */

#ifndef __ESKNODO_STORE_H__
#define __ESKNODO_STORE_H__

#include "MoistureSensor1.h"
#include <Arduino.h>

class Store {
    private:
        void setup(unsigned int size);
    public:
        Store();
        bool shutdown();
        bool commit();
        bool wipe();
        // The neatest way to access variables stored in EEPROM is using a structure
        struct EEPROMStruct {
            /** counts wake-ups. If this count reaches schedules sleep cycles, the programm gets active */
            int wakeupCounter = 0;
            /**
             * This fraction of 1024 is added to each sleep cycle duration.
             * It can be negative, then this value is subtracted from sleep cycle seconds
             * Without Dimension, is a fraction of 1/1024
             */
            int clockAdjustment = 77;
            /**
             * The count of wakeups to elapse before the program is executed.
             * A better name could be "inactive cycles".
             * The length of the full cycle is N sleepCycles + 1 active cycle.
             * The active cycle does not count for the sleep cycles.
             * With sleep cycles = 0, every cycle is an active cycle.
             * If this value ist set to != 0, it overrides the battery-calculated value.
             */
            int sleepCycles = 1;
            /**
             * Collection of flags for e.g. development.
             */
            int developOptions = 0;
            /**
             * hold min,max values of 2 sensors.
             * Usage: moistureCalibration[SENSOR_ID][MIN_MOIST=0,MAX_MOIST=1]
             *
             * Max Value: Raw sensor value for 100% humidity.
             * Since maximum moisture conditions would result in lowest
             * frequency values, this is the lower border of the moisture range.
             *
             * Min Value: Raw sensor value for 0% humidity.
             * Since minimum moisture conditions would result in highest
             * frequency values, this is the upper border of the moisture range.
             */
            int moistureCalibration[2][2];
        } data;
};

#endif
