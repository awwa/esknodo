/**
 * Represents a  moisture sensor with frequency output (Sensor type 2).
 * Compared to type 1, the pulse and space duration is measured instead of the frequency.
 */
#include <inttypes.h>
#include <limits.h>
#include <Arduino.h>
#include "MoistureSensor2.h"
#include "Store.h"
#include "esknodo.h"


/**
 * Permanent EEPROM store
 * Usage: Store.data.VARNAME
 */
extern Store EEStore;

extern int developOptions;


MoistureSensor2::MoistureSensor2()
{
}

/**
 * Sets up signal pin and sensor id.
 */
MoistureSensor2::MoistureSensor2(int moistPin, byte sensorId)
{
    this->sensorId = sensorId;
    this->setup(moistPin);
}

/**
 * Sets portpin, sets up frequency measurement object.
 */
void MoistureSensor2::setup(int moistPin)
{
    this->portPin = moistPin;
    pinMode(moistPin, INPUT);
    this->updateFromEEStore();
}

/**
 * Updates sensor settings from EEStore
 */
void MoistureSensor2::updateFromEEStore()
{
    this->minimumMoistureRaw = EEStore.data.moistureCalibration[this->sensorId][MIN_MOISTURE_INDEX];
    this->maximumMoistureRaw = EEStore.data.moistureCalibration[this->sensorId][MAX_MOISTURE_INDEX];
}

/**
 * Sets default calibration values
 */
void MoistureSensor2::setDefaultCalibrationValues()
{
    this->setMoistureMaxConfig(DEFAULT_MAX_MOISTURE2);
    this->setMoistureMinConfig(DEFAULT_MIN_MOISTURE2);
}

/**
 *
 */
byte MoistureSensor2::getSensorId()
{
    return this->sensorId;
}

/**
 * Set the calibration value for maximum moisture
 * Updates also Estore.
 * @param maxValue
 */
void MoistureSensor2::setMoistureMaxConfig(int maxValue)
{
    this->maximumMoistureRaw = maxValue;
    EEStore.data.moistureCalibration[this->sensorId][MAX_MOISTURE_INDEX] = maxValue;
}
/**
 * Get the value of maximum moisture
 */
int MoistureSensor2::getMoistureMaxConfig()
{
    return this->maximumMoistureRaw;
}

/**
 * Set the calibration value for minimum moisture
 * Updates also Estore.
 * @param minValue
 */
void MoistureSensor2::setMoistureMinConfig(int minValue)
{
    this->minimumMoistureRaw = minValue;
    EEStore.data.moistureCalibration[this->sensorId][MIN_MOISTURE_INDEX] = minValue;
}

/**
 * Get the calibration value of minimum moisture
 */
int MoistureSensor2::getMoistureMinConfig()
{
    return this->minimumMoistureRaw;
}


int MoistureSensor2::getMoisture()
{
    int raw, normalized;

    raw = this->getRawMoisture();
    normalized = this->rawToNormalized(raw);

    return normalized;
}


int MoistureSensor2::getRawMoisture()
{
    return this->markDurationAccumulator / MS2_MOVING_AVERAGE_FRACTION;
}

/**
 * Calculates percentage moisture from raw sensor frequency.
 * @return Percent value of moisture
 */
int MoistureSensor2::rawToNormalized(int raw)
{
    int slope, normalized;
    const int expandFactor = 64;

    if(raw == 0) {
        return ERR_RAW_VALUE_IS_ZERO;
    }

    // margin cases: raw value out of min-max borders
    if(raw < this->minimumMoistureRaw) {
        // more dry than expected
        return ERR_MOISTURE_IS_NEGATIVE;
    }
    if(raw > this->maximumMoistureRaw) {
        // more wet than calibrated
        // auto-calibrate maximum value
        // the absolute step is relative to the calibration values
        int calibrationStep = ((this->maximumMoistureRaw - this->minimumMoistureRaw) / 100) * MAX_MOIST_AUTO_CALIB_STEPUP;
        if (calibrationStep == 0) {
            calibrationStep = 1;
        }
        this->setMoistureMaxConfig(this->getMoistureMaxConfig() + calibrationStep);
        return ERR_MOISTURE_IS_OVERFLOW;
    }
    if (this->maximumMoistureRaw == this->minimumMoistureRaw) {
        // misconfiguration
        return ERR_MIN_AND_MAX_CONFIG_ARE_EQUAL;
    }

    // calculate factor m
    slope = 100 * expandFactor / (this->maximumMoistureRaw - this->minimumMoistureRaw);

    // calculate offset b
    int offset = slope * this->minimumMoistureRaw;
    if (developOptions & DevSensorVerbose) {
        Serial.print("slope=");
        Serial.println(slope);
        Serial.print("offset=");
        Serial.println(offset);
    }

    // apply mx + b equation
    normalized = slope * raw - offset;
    // deflate result
    normalized /= expandFactor;
    if (developOptions & DevSensorVerbose) {
        Serial.print("raw normalized=");
        Serial.println(normalized);
    }

    // assure that reserved values are not output of this calculation
    if (normalized <= ERR_MOISTURE_IS_NEGATIVE) {
        normalized = ERR_MOISTURE_IS_NEGATIVE;
    }
    if (normalized >= ERR_MOISTURE_IS_OVERFLOW-1) {
        normalized = ERR_MOISTURE_IS_OVERFLOW-1;
    }

    return normalized;
}

/**
 * Counts signal mark and space duration.
 * Notifiys duration on signal edges.
 * @todo consider overflow of counter in case of no signal
 */
void MoistureSensor2::tick1ms()
{
    if(digitalRead(this->portPin)) {
        // High Level
        if(this->debounceCounter >= this->debounceValue) {
            this->markDurationCounter ++;
        }
        else {
            this->debounceCounter ++;
            if(this->debounceCounter >= this->debounceValue) {
                // High Level was reached now
                this->edgeCounts++;
                this->notifySpaceDuration(this->spaceDurationCounter);
                this->spaceDurationCounter = 0;
            }
        }
    }
    else {
        // Low Level
        if(this->debounceCounter <= 0) {
            this->spaceDurationCounter ++;
        }
        else {
            this->debounceCounter --;
            if(this->debounceCounter <= 0) {
                // Low Level was reached now
                this->edgeCounts ++;
                this->notifyMarkDuration(this->markDurationCounter);
                this->markDurationCounter = 0;
            }
        }
    }
}

/**
 * Generic function to register the measurement of the mark duration
 */
void MoistureSensor2::notifyMarkDuration(uint16_t markDuration)
{
    if(this->markDurationAccumulator == 0) {
        // fast init of accumulator
        this->markDurationAccumulator = markDuration * MS2_MOVING_AVERAGE_FRACTION;
    }
    else {
        uint16_t fraction = this->markDurationAccumulator / MS2_MOVING_AVERAGE_FRACTION;
        this->markDurationAccumulator -= fraction;
        this->markDurationAccumulator += markDuration;
    }
}

/**
 * Accumulates space duration values
 */
void MoistureSensor2::notifySpaceDuration(uint16_t spaceDuration)
{
    if(this->spaceDurationAccumulator == 0) {
        // fast init of accumulator
        this->spaceDurationAccumulator = spaceDuration * MS2_MOVING_AVERAGE_FRACTION;
    }
    else {
        uint16_t fraction = this->spaceDurationAccumulator / MS2_MOVING_AVERAGE_FRACTION;
        this->spaceDurationAccumulator -= fraction;
        this->spaceDurationAccumulator += spaceDuration;
    }
}

/**
 * Tells if measurements are ready.
 *
 * This supports kind of "warm up" behaviour.
 *
 * @param measurementsCount 0 ist fastest startup, default is 3
 * @return true
 * @return false
 */
bool MoistureSensor2::isReady(int measurementsCount)
{
    if (this->edgeCounts / 2 > measurementsCount * MS2_MOVING_AVERAGE_FRACTION) {
        return false;
    }
    else {
        return true;
    }
}

/**
 * Returns an informative string
 */
String MoistureSensor2::getDebugInfo()
{
    char buffer[64];
    sprintf(buffer, "%u%% (%u) [%u,%u]", this->getMoisture(), this->getRawMoisture(), this->getMoistureMinConfig(), this->getMoistureMaxConfig() );
    return String(buffer);
}
