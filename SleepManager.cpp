/**
 * Manages sleep time and wakeup.
 * To manage sleep and program activity, there is a sleep time setting and a sleep cycle counter which counts the number of wakeups.
 * Sleep time depends on develop mode: production: 60min, develop: 1min (maximum is ~71min)
 *
 * Sleep cycles depend on battery state
 * Battery -> develop / production
 * 100% -> 0cycles -> every minute / hour
 * 99% -> 4cycles -> every 5 minutes / hours
 * 1% -> 168cycles -> every 3 hours / 1 week
 *
 * The battery level is evaluataed on every wakeup.
 * That means, if we have a high count of inactivity cycles due to low battery,
 * this cycles will decrease if the battery level improves by e.g. charging.
 *
 * @todo for every component, a "beforeSleeping()" method should be available
 *
 */
#include <inttypes.h>
#include <Arduino.h>

#include "SleepManager.h"
#include "esknodo.h"

#include "Store.h"
#include "Mqtt.h"
#include "MoistureSensor1.h"

#include <Wire.h>
#include <PubSubClient.h>
#include <ESP_EEPROM.h>
#include <ESPBattery.h>

#include <limits.h>
#include <Arduino.h>

#define TIME_60MIN_U_SECONDS 3600000000
#define TIME_1MIN_U_SECONDS 60000000

extern int developOptions;
extern void setPeripheralBusPower(bool power);

// The following components must be informed about sleep:

/**
 * For sleep, store writes unsaved data to EEPROM
 */
extern Store EEStore;
/**
 * For sleep, Mqtt disconnects from server
 */
extern PubSubClient MqttClient;
/**
 * For sleep, sensors are powered down via bus power
 */
/**
 * Is needed to calculate sleep cycles
 */
extern ESPBattery Battery;

/**
 * Sets sleep time.
 * development: 1min
 * production: 60min
 */
SleepManager::SleepManager()
{
    if (developOptions & DevShortSleep) {
        this->sleepTimeSeconds = 1*60;
    }
}

/**
 * Calculates and returns sleep seconds
 */
uint32_t SleepManager::getSleepTimeSeconds()
{
    if (developOptions & DevShortSleep) {
        this->sleepTimeSeconds = 1*60;
    }
    else {
        this->sleepTimeSeconds = 60*60;
    }
    return this->sleepTimeSeconds;
}

/**
 * Returns calculated idle cycles
 */
int SleepManager::getCalculatedSleepCycles()
{
    int batteryPercentage = Battery.getPercentage();
    Serial.printf("battery volt=%.2f (%3d%%)\r\n", Battery.getVoltage(), batteryPercentage);
    // Evaluate battery level and calculate sleep cycles
    this->calculatedSleepCycles = this->batteryPercentToSleepCycles(batteryPercentage);
    return this->calculatedSleepCycles;
}


int SleepManager::manageWakeupCycles(bool shutdown)
{
    return manageWakeupCycles(shutdown, 0);
}


/**
 * Sends the system to sleep depending on sleeps cycles.
 *
 * Sleep cycles are counted with the wakeup counter. If required
 * count of sleep cycles is not reached, the system is put to sleep and the
 * wakeup counter is incremented.
 * If it reaches the preset sleep cycles, it is reset to 0 and the
 * execution continues in every case.
 * If shutdown is set to true, this function is authorised to shut down the
 * system if wakeup counter has not reached sleep cycles.
 * If the function returns, it returns the wakeup counter.
 * @param bool True if the programm should shut down. Then execution ends in this function.
 * @param uint16 Time to wait before process shutdown
 * @return int Returns the count of elapsed wakeups. Returns 0 if the programm should be executed.
 */
int SleepManager::manageWakeupCycles(bool shutdown, uint16_t delay)
{
    int sleepCycles;

    // wait delay
    if(delay) {
        uint32_t now = millis();
        this->isInShutdownFlag = true;
        Serial.printf("wait delay %u ", delay);
        while(millis() < now + delay) {
            // wait delay
            yield();
            if(this->cancelShutdownFlag) {
                Serial.println("");
                Serial.println("shutdown cancelled, return 0");
                return 0;
            }
        }
        this->isInShutdownFlag = false;
        Serial.println("timed out");
    }

    // check sleep/wakeup status
    if (developOptions & DevNoSleep) {
        Serial.println("Option: no sleep");
    }
    else {
        // Serial.print("EEPROM wakeup counter=");
        // Serial.println();
        Serial.printf("EEPROM wakeup counter= '%u'\r\n", EEStore.data.wakeupCounter);

        // if sleep cycle value is set in eeprom, this value overrides the battery-calculated value
        if (EEStore.data.sleepCycles) {
            sleepCycles = EEStore.data.sleepCycles - 1;
            // Serial.print("programmed sleep cycles=");
            // Serial.println(sleepCycles);
            Serial.printf("programmed sleep cycles='%u'\r\n", sleepCycles);
        }
        else {
            // if no sleep cycle value is set in eeprom, the sleep cycles are calculated from battery level
            sleepCycles = this->getCalculatedSleepCycles();
            Serial.print("battery level sleep cycles=");
            Serial.println(sleepCycles);
        }

        if(EEStore.data.wakeupCounter >= sleepCycles) {
            // runs payload if wakeup counter (which counts sleep cycles) exceeds scheduled sleep cycles
            Serial.println("count of wakeups has reached programmed sleep cycles");
            EEStore.data.wakeupCounter = 0;
        }
        else {
            // active cycle not reached
            EEStore.data.wakeupCounter++;
            Serial.print("EEPROM wakeup counter incremented to ");
            Serial.println(EEStore.data.wakeupCounter);
            if (shutdown == true) {
                Serial.println("shutdown requested");
                this->shutdown();
            }
        }
    }

    return EEStore.data.wakeupCounter;
}

/**
 *  Tells if current cycle is active cycle
 */
bool SleepManager::isActiveCycle()
{
    if(EEStore.data.wakeupCounter == 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * 100% -> 1 cycle -> every 3hours/3minutes
 * 60% - 20% every 24hours/24minutes
 * 1% -> 168 cycles -> every week/3hours
 *
 * @param batteryPercent
 * @return int
 */
int SleepManager::batteryPercentToSleepCycles(int batteryPercent)
{
    int sleepCycles = 0;

    if (batteryPercent <= 0) {
        // sleep with no wake up
        sleepCycles = -1;
    }
    else if (batteryPercent > 50) {
        // 3 mins / hours
        sleepCycles = 3;
    }
    else if (batteryPercent > 20) {
        // 24 / 1day
        sleepCycles = 24;
    }
    else {
        // 3 hours / 1 week
        sleepCycles = 168;
    }

    return sleepCycles;
}

/**
 * Shuts down for calculated amount of time.
 * @todo allow to set wifi mode at shutdown
 */
void SleepManager::shutdown()
{
    this->shutdown(this->getSleepTimeSeconds());
}


void SleepManager::shutdownFinally()
{
    this->shutdown(0);
}


void SleepManager::shutdown(uint32_t sleepSeconds)
{
    if(sleepSeconds) {
        Serial.print("entering deep sleep for sec: ");
        Serial.println(sleepSeconds);
    }
    else {
        Serial.println("shutting down forever");
    }
    Serial.println();
    Serial.println();
    MqttClient.disconnect();
    Wire.endTransmission(true);
    setPeripheralBusPower(SENSOR_POWER_OFF);
    EEStore.shutdown();
    ESP.deepSleep(this->adjustSleepSeconds(sleepSeconds*1000*1000));
    // yield is also called within deepSleep()
    while(true) {
        yield();
    }

    /// @todo
    //ESP.deepSleep(1, WAKE_RF_DEFAULT);
    //###########################################
    //ESP.deepSleep(1, WAKE_);
    /*
    -WAKE_RF_DEFAULT(aka deepsleepsetoption0): do or not do the radio calibration depending on the init byte 108.
    -WAKE_RFCAL(aka deepsleepsetoption1): do the radio calibration every time.
    -WAKE_NO_RFCAL(aka deepsleepsetoption2): do NOT the radio calibration on wake up.
    -WAKE_RF_DISABLED(aka deepsleepsetoption3): on wake up DISABLE the modem. So for example I can't connect the esp to wifi.
    */
}

/**
 * Adjusts internal clock
 */
unsigned long SleepManager::adjustSleepSeconds(unsigned long microseconds)
{
    /**
     * this fraction is to add to the given microseconds
     * it may be negative
     */
    int adjustmentFraction1024 = EEStore.data.clockAdjustment;
    long adjustmentMicroseconds = microseconds/1024;
    adjustmentMicroseconds *= adjustmentFraction1024;
    microseconds += adjustmentMicroseconds;
    Serial.printf("Sleep adjusted by %.4f ms\r\n", (float)(adjustmentMicroseconds/1000));

    return microseconds;
}


bool SleepManager::isInShutdown()
{
    return this->isInShutdownFlag;
}

void SleepManager::cancelShutdown()
{
    this->cancelShutdownFlag = true;
}
