/**
 * Represents a pin with a measurable frequency at its input.
 * Level transitions are counted for a certain time. This gives the raw value.
 */
#include "FrequencySignal.h"
#include "esknodo.h"
#include <Arduino.h>

extern int developOptions;

/**
 * Inits without pin.
 */
FrequencySignal::FrequencySignal()
{
}

/**
 * Inits with pin.
 */
FrequencySignal::FrequencySignal(int portPin)
{
    this->setup(portPin);
}

/**
 * Sets port pin.
 */
void FrequencySignal::setup(int portPin)
{
    this->portPin = portPin;
    pinMode(portPin, INPUT);
    if (developOptions & DevSensorVerbose) {
        Serial.println("setup frequency port pin=" + String(this->portPin));
    }
}

/**
 * Gets port pin.
 */
int FrequencySignal::getPortPin()
{
    return this->portPin;
}

/**
 * Increment measurement counter on signal polarity transition
 */
void FrequencySignal::tick1ms()
{
    bool currentPinState = digitalRead(this->portPin);

    if (this->memoPinState != currentPinState) {
        if (this->countedLevelTransitions < INT16_MAX) {
            this->countedLevelTransitions ++;
        }
    }
    this->memoPinState = currentPinState;
    this->elapsedTicks ++;

    // store current measurement and prepare a new one
    if (this->elapsedTicks >= this->ticksToMeasure) {
        // two transitions give one full cycle:
        this->raw = this->countedLevelTransitions/2;
        this->elapsedTicks = 0;
        this->countedLevelTransitions = 0;
        if (this->measurementsCounter < INT16_MAX) {
            this->measurementsCounter ++;
        }
            
        if (developOptions & DevSensorVerbose & DevMoreVerbose) {            
            Serial.print("measurement cycle completed of pin: ");
            Serial.println(this->portPin);
            Serial.print("raw=");
            Serial.print(this->raw);
            Serial.print(" measurement-count=");
            Serial.println(this->measurementsCounter);
            Serial.print(" current: ");
            Serial.print(currentPinState);
            Serial.print(" memo: ");
            Serial.println(this->memoPinState);
        }
    }
}


int FrequencySignal::getRawValue()
{
    return this->raw;
}


unsigned int FrequencySignal::getMeasurementCounter()
{
    return this->measurementsCounter;
}
