
#ifndef __SystemLed_H__
#define __SystemLed_H__

class SystemLed {
private:
    int port;
    /**
     * If true, setting a high level darkens the LED.
     */
    bool logicLevelInverted = false;
    unsigned int counter;
    unsigned int ledOnTime;
    unsigned int ledOffTime;
    bool logicLevel(bool level);
    int blinkMode;
    int blinkCodeCount;
    int queuedBlinkCode;
    int waitAfterBlinkTime;
    int flashBeforeBlink;
    int pauseBefore;

public:
    SystemLed(int port, bool inverted);
    void setup();
    bool setBlinkCode(int blinkCode);
    bool setErrorCode(int errorCode);
    bool setBlinking(int blinkMode);
    bool setSingleOn(int onTime);
    bool setSingleOn(int onTime, int offTime);
    bool on();
    bool off();
    void routine1ms();
    void routine100ms();
    void loop();
    bool isReady();
};

#endif
