/**
 * contains globals for esknodo
 *
 */

#ifndef __ESKNODO_H__
#define __ESKNODO_H__

#include <Arduino.h>

/**
 * Placeholder for no options.
 */
#define DevNoDevelopmentOptions        0
/**
 * Override built-in options from the programm with EEStore options by setting a void option.
 */
#define DevOverrideOptions             (1 << 0)
/**
 * Sets serial monitor to 74880 baud.
 * This odd speed will show ESP8266 boot diagnostics.
 * @todo its mqtt variable could be replaced with other, more meaningful settings
 */
#define DevSerialSpeed74880ForBootMsg  (1 << 1)
/**
 * At no sleep, mqtt statements are sent by keypress
 * @todo can be replaced by special sleep interval value
 */
#define DevNoSleep                     (1 << 2)
/**
 * Triggers sending of config values next cycle.
 */
#define DevPublishConfig         (1 << 3)
/**
 * Normally, a sleep period lasts 60 min,
 * in develop mode a short sleep of 1 min is used
 */
#define DevShortSleep                  (1 << 4)
/**
 * Shows passwords and configuration in log messages
 */
#define DevShowConfidentials           (1 << 5)
/**
 * Shows more log messages than normal
 */
#define DevMoreVerbose                 (1 << 6)
/**
 * Shows more verbose sensor messgages
 */
#define DevSensorVerbose               (1 << 7)

// Error blink codes
#define BLINKCODE_BATTERY_LOW   2
#define BLINKCODE_SLEEPCYCLE    3
#define BLINKCODE_SERIAL_FAILED 4
#define BLINKCODE_WIFI_FAILED   5
#define BLINKCODE_WIFI_TIMEOUT  6
#define BLINKCODE_NO_MQTT       7

// MQTT topics: these are shared among all esknodo instances
#define MQTT_TOPIC_POSTFIX_CONFIG "config"
#define MQTT_TOPIC_POSTFIX_DEVEL "devel"
#define MQTT_TOPIC_POSTFIX_SENSOR "sensor"
#define MQTT_TOPIC_POSTFIX_SYSTEM "system"
#define MQTT_TOPIC_CALIBRATION_SUBSCRIPTION "calib/#"
#define MQTT_TOPIC_CALIBRATION_QUERY "calib"

/* MQTT defaults. Individual values are set in portal setup */
#define MQTT_SERVER_DEFAULT        "my.mqtt-broker.com"
#define MQTT_PORT_DEFAULT          1883
#define MQTT_CLIENTID_DEFAULT      "myclientid"
#define MQTT_USERNAME_DEFAULT      "myuser"
#define MQTT_PASSWORD_DEFAULT      "mypassword"
#define MQTT_TOPIC_DEFAULT         "my/topic/"
#define MQTT_QOS_DEFAULT           0
#define MQTT_CLEAN_SESSION_DEFAULT 1

// additional battery levels
#define ESPBATTERY_FULL_HIGH    700
#define ESPBATTERY_FULL_MAX     730


/** @todo this should be moved to a class */
String developOptionsToString(int developOptions);
void executeBoolExpressionOnDevelOption(int boolExpression, int develOption);

#endif
