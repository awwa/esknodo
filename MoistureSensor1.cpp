/**
 * Represents a  moisture sensor with frequency output (Sensor type 1).
 * The power enable pin is shared with all sensors.
 * This class uses the FrequencySignal class to measure the sensor signal frequency.
 */
#include <limits.h>
#include <Arduino.h>
#include "MoistureSensor1.h"
#include "Store.h"
#include "esknodo.h"


/**
 * Permanent EEPROM store
 * Usage: Store.data.VARNAME
 */
extern Store EEStore;

extern int developOptions;


MoistureSensor1::MoistureSensor1()
{
}

/**
 * Sets up signal pin and sensor id.
 */
MoistureSensor1::MoistureSensor1(int moistPin, byte sensorId)
{
    this->sensorId = sensorId;
    this->setup(moistPin);
}

/**
 * Sets portpin, sets up frequency measurement object.
 */
void MoistureSensor1::setup(int moistPin)
{
    this->updateFromEEStore();
    Moisture = FrequencySignal(moistPin);
}

/**
 * Updates sensor settings from EEStore
 */
void MoistureSensor1::updateFromEEStore()
{
    this->minimumMoistureRaw = EEStore.data.moistureCalibration[this->sensorId][MIN_MOISTURE_INDEX];
    this->maximumMoistureRaw = EEStore.data.moistureCalibration[this->sensorId][MAX_MOISTURE_INDEX];
}

/**
 * Sets default calibration values
 */
void MoistureSensor1::setDefaultCalibrationValues()
{
    this->setMoistureMaxConfig(2000);
    this->setMoistureMinConfig(8000);
}

/**
 *
 */
byte MoistureSensor1::getSensorId()
{
    return this->sensorId;
}

/**
 * Set the calibration value for maximum moisture (wet state = minimum frequency)
 * Updates also Estore.
 * @param maxValue
 */
void MoistureSensor1::setMoistureMaxConfig(int maxValue)
{
    this->maximumMoistureRaw = maxValue;
    EEStore.data.moistureCalibration[this->sensorId][MAX_MOISTURE_INDEX] = maxValue;

    if (developOptions & DevSensorVerbose) {
        Serial.println("Set sensor moisture min/max now=" + String(this->getMoistureMinConfig()) + " " + String(this->getMoistureMaxConfig()));
    }
}
/**
 * Get the value of maximum moisture (minimum frequency = minimum moisture = dry state)
 */
int MoistureSensor1::getMoistureMaxConfig()
{
    return this->maximumMoistureRaw;
}

/**
 * Set the calibration vallue for minimum moisture (wet state = minimum frequency)
 * Updates also Estore.
 * @param minValue
 */
void MoistureSensor1::setMoistureMinConfig(int minValue)
{
    this->minimumMoistureRaw = minValue;
    EEStore.data.moistureCalibration[this->sensorId][MIN_MOISTURE_INDEX] = minValue;
    if (developOptions & DevSensorVerbose) {
        Serial.println("Sensor moisture min/max now=" + String(this->getMoistureMinConfig()) + " " + String(this->getMoistureMaxConfig()));
    }
}

/**
 * Get the calibration value of minimum moisture (dry state = max frequency)
 */
int MoistureSensor1::getMoistureMinConfig()
{
    return this->minimumMoistureRaw;
}


int MoistureSensor1::getRawMoisture()
{
    return Moisture.getRawValue();
}


int MoistureSensor1::getMoisture()
{
    int m, normalized;
    int raw = Moisture.getRawValue();
    if (developOptions & DevSensorVerbose) {
        Serial.println("moisture raw value from pin "+String(this->Moisture.getPortPin())+" = "+String(raw));
    }
    if(raw == 0) {
        return ERR_RAW_VALUE_IS_ZERO;
    }
    else {
        normalized = this->rawToNormalized(raw);
        // do not return big negative values as they may distort line charts
        if (normalized < 0) {
            normalized = ERR_MOISTURE_IS_NEGATIVE;
        }
        return normalized;
    }
}

/**
 * Calculates percentage moisture from raw sensor frequency.
 *
 * - assumes raw frequency from 400 to 1000
 * - maximum moisture raw varies and is sensor specific
 * - based on linear equation: y = mx + b
 *
 * -> m = 100 / (minMoisture - maxMoisture)
 * -> m = -(1/6)
 * -> b = -mx   | x=1000
 * -> b = -m * maxMoisture
 * m and b are inflated by factor 100 to allow integer math
 * Error conditions are returned as negative numbers.
 * @return Percent value of moisture
 */
int MoistureSensor1::rawToNormalized(int raw)
{
    int m, normalized;
    const int expandFactor = 64;

    if(raw == 0) {
        return ERR_RAW_VALUE_IS_ZERO;
    }

    if(this->maximumMoistureRaw >= this->minimumMoistureRaw) {
        // this is a misconfiguration
        return ERR_MAX_BORDER_IS_HIGHER_THAN_MIN_BORDER;
    }

    // margin cases: raw value out of min-max borders
    if(raw > this->minimumMoistureRaw) {
        // more dry than expected
        return ERR_MOISTURE_IS_NEGATIVE - 1;
    }
    if(raw < this->maximumMoistureRaw) {
        // more wet than calibrated
        // auto-calibrate maximum value
        // the absolute step is relative to the calibration values
        int calibrationStep = ((this->minimumMoistureRaw - this->maximumMoistureRaw) / 100) * MAX_MOIST_AUTO_CALIB_STEPUP;
        if (calibrationStep == 0) {
            calibrationStep = 1;
        }
        this->setMoistureMaxConfig(this->getMoistureMaxConfig() - calibrationStep);
        return ERR_MOISTURE_IS_OVERFLOW;
    }

    // calculate factor m
    m = 100 * expandFactor / (this->maximumMoistureRaw - this->minimumMoistureRaw);
    if (developOptions & DevSensorVerbose) {
        Serial.print("m-value=");
        Serial.println(m);
    }

    // calculate offset b
    int b = -m * this->minimumMoistureRaw;
    if (developOptions & DevSensorVerbose) {
        Serial.print("b-value=");
        Serial.println(b);
    }

    // apply mx + b equation
    normalized = m * raw + b;
    // deflate result
    normalized /= expandFactor;
    if (developOptions & DevSensorVerbose) {
        Serial.print("raw normalized=");
        Serial.println(normalized);
    }

    // assure that reserved values are not output of this calculation
    if (normalized <= ERR_MOISTURE_IS_NEGATIVE) {
        normalized = ERR_MOISTURE_IS_NEGATIVE;
    }
    if (normalized >= ERR_MOISTURE_IS_OVERFLOW - 1) {
        normalized = ERR_MOISTURE_IS_OVERFLOW - 1;
    }

    return normalized;
}

void MoistureSensor1::tick1ms()
{
    Moisture.tick1ms();
}

/**
 * Tells if measurements are ready.
 *
 * measurementsCount = 0 means: dont care for the count
 * of measurements, sensor is always "ready"
 *
 * This supports kind of "warm up" behaviour.
 *
 * @param measurementsCount
 * @return true
 * @return false
 */
bool MoistureSensor1::isReady(int measurementsCount)
{
    if (Moisture.getMeasurementCounter() < measurementsCount) {
        return false;
    }
    else {
        return true;
    }
}

/**
 * Returns an informative string
 */
String MoistureSensor1::getDebugInfo()
{
    String debugInfo = String("moisture-sensor-id"+String(this->getSensorId())+"="+String(this->getMoisture())+" raw="+String(this->getRawMoisture())+" cfg min/max="+String(this->getMoistureMinConfig())+"/" + String(this->getMoistureMaxConfig()) );
    return debugInfo;
}


