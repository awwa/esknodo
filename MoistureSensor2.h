/**
 */

#ifndef __SENSOR2_H__
#define __SENSOR2_H__

#include <inttypes.h>
#include <Arduino.h>

#include "install/hardware.h"
#include "MoistureSensor12.h"

#define DEFAULT_MAX_MOISTURE2 10000
#define DEFAULT_MIN_MOISTURE2 100
/**
 * The amount of the measurement in the accumulator given as fraction
 */
#define MS2_MOVING_AVERAGE_FRACTION  4



class MoistureSensor2 {
private:
    /**
     * Holds sensor ID to access sensor config etc.
     */
    byte sensorId;
    byte portPin;
    /**
     * The value must be greater than 0, else the measurment does not work
     */
    byte debounceValue = 1;
    byte debounceCounter;
    int16_t edgeCounts;
    uint16_t currentDurationCounter;
    uint16_t markDurationAccumulator;
    uint16_t spaceDurationAccumulator;
    uint16_t markDurationCounter;
    uint16_t spaceDurationCounter;
    void notifyMarkDuration(uint16_t markDuration);
    void notifySpaceDuration(uint16_t spaceDuration);

public:
    MoistureSensor2();
    MoistureSensor2(int portPin, byte sensorId);
    void tick1ms();
    void setup(int PortPin);
    byte getSensorId();
    /**
     * The value for maximum (100%, wet) moisture.
     * It can be adjusted via mqtt and is stored in eeprom
     */
    int maximumMoistureRaw = DEFAULT_MAX_MOISTURE2;
    /**
     * The value for minimum (0% moisture, dry) condition.
     * Setting the "dry" value to the "free air" value would result in poor sensitivity of the sensor.
     * Therefore, the minimum moisture value should be tested with dry soil.
     */
    int minimumMoistureRaw = DEFAULT_MIN_MOISTURE2;

    void updateFromEEStore();
    void setDefaultCalibrationValues();

    int getRawMoisture();
    int rawToNormalized(int raw);
    int getMoisture();

    void setMoistureMaxConfig(int maxValue);
    int getMoistureMaxConfig();
    void setMoistureMinConfig(int minValue);
    int getMoistureMinConfig();

    bool isReady(int measurementsCount = 3);
    String getDebugInfo();
};

#endif
